package com.mindtree.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Customer {
	@Id
private int id;

private String name;
private int feedback;
public Customer() {
	super();
	// TODO Auto-generated constructor stub
}
public Customer(int id, String name, int feedback) {
	super();
	this.id = id;
	this.name = name;
	this.feedback = feedback;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getFeedback() {
	return feedback;
}
public void setFeedback(int feedback) {
	this.feedback = feedback;
}
@Override
public String toString() {
	return "Customer [id=" + id + ", name=" + name + ", feedback=" + feedback + "]";
}

}
