package com.mindtree.jpaonentity;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.mindtree.entity.Customer;
import com.mindtree.service.CustomerService;

/**
 * Hello world!
 *
 */
public class App {
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {

		int choice = 0;

		do {
			menu();

			choice = scan.nextInt();
			switch (choice) {
			case 1:
				List<Customer> customer = CustomerService.insertCustomers();
				if(customer==null)
				System.out.println(" not inserted");
				else
					System.out.println(customer);
				break;
			case 2:
				System.out.println("enter id");
				int id = scan.nextInt();
				Customer c = CustomerService.displayCustomerById(id);
				if (c == null) {
					System.out.println("id is not found");
				} else {
					System.out.println("id is found"+c);
				}
				break;
			case 3:
				List<Customer> cus = CustomerService.displayCustomers();
				if(cus==null)
				System.out.println("no details to display");
				else
					System.out.println(cus);

			break;
			case 4:
				System.out.println("enter id to to update");
				int id1 = scan.nextInt();
				System.out.println("enter name to update");
				String name1 = scan.next();
				System.out.println("enter feedbcak to update");
				int feedback1 = scan.nextInt();
				Customer customer1 = CustomerService.updateAllDetailsById(id1, name1, feedback1);
				if(customer1==null)
				{
					System.out.println("id not found");
				}
				else
					System.out.println(customer1);
				break;
			case 5:
				System.out.println("enter id to delete all details");
				int id3 = scan.nextInt();
				Customer customer2=CustomerService.deleteAllDetailsById(id3);
				if(customer2==null)
				{
					System.out.println("id not found");
				}
				else
					System.out.println(customer2);
				break;
			case 6:
				System.out.println("exited");
				break;
			default:
				System.out.println("invalid choice");
			}
		} while (choice != 0);
	}

	public static List<Customer> createCustomers() {
		// TODO Auto-generated method stub
		System.out.println("enter number of customers");
		int noOfCustomers = scan.nextInt();
		List<Customer> customer = new ArrayList<Customer>();
		for (int i = 0; i < noOfCustomers; i++) {
			System.out.println("enter customer id");
			int id = scan.nextInt();
			System.out.println("enter customer name");
			String name = scan.next();
			System.out.println("enter feedback");
			int feedback = scan.nextInt();
			Customer c = new Customer(id, name, feedback);
			customer.add(c);
		}
		return customer;
	}

	private static void menu() {
		// TODO Auto-generated method stub
		System.out.println("*******MENU******************");
		System.out.println("1.insert customers in db");
		System.out.println("2.Display customer details by id");
		System.out.println("3.display customer details");
		System.out.println("4.update customer details by given id");
		System.out.println("5.delete customer details by id");
		System.out.println("****************************************");
	}
}
