package com.mindtree.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.mindtree.entity.Customer;
import com.mindtree.jpaonentity.App;

public class CustomerService {

	public static List<Customer> insertCustomers() {
		// TODO Auto-generated method stub
		List<Customer> customer = App.createCustomers();

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
		EntityManager em = emf.createEntityManager();
		for (Customer i : customer) {
			int id = i.getId();
			String name = i.getName();
			int feedback = i.getFeedback();

			Customer c = new Customer(id, name, feedback);
			em.getTransaction().begin();
			em.persist(c);
			em.getTransaction().commit();

		}
		return customer;
	}

	public static Customer displayCustomerById(int id) {

		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
		EntityManager em = emf.createEntityManager();
		// Customer c=new Customer();
		Customer c = em.find(Customer.class, id);
		return c;

	}

	public static Customer updateAllDetailsById(int id1, String name1, int feedback1) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		List<Customer> customers=new ArrayList<Customer>();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
		EntityManager em = emf.createEntityManager();
//		if(id1==em.find(Customer.class,customers.get(id1)))
//		{
		Customer c = new Customer();
		c.setId(id1);
		c.setName(name1);
		c.setFeedback(feedback1);
		em.getTransaction().begin();
		em.merge(c);
		em.getTransaction().commit();
		Customer c1 = em.find(Customer.class, id1);
		return c1;

	}
	

	public static Customer deleteAllDetailsById(int id3) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
		EntityManager em = emf.createEntityManager();
		Customer ref = em.getReference(Customer.class, id3);
		em.getTransaction().begin();
		em.remove(ref);
		em.getTransaction().commit();
		em.close();
		emf.close();
		return ref;
	}

	public static List<Customer> displayCustomers() {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
		EntityManager em = emf.createEntityManager();
		String hql="select c from Customer c";
		Query query=em.createQuery(hql);
		List<Customer> l=query.getResultList();
		return l;
		
	}

//	
}
